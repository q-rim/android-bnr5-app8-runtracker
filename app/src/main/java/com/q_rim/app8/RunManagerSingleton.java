package com.q_rim.app8;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;

/*
 * this singleton manages the communication with LocationManager
 */
public class RunManagerSingleton {
  private static final String TAG = "Q---RunManagerSingleton:";

  public static final String ACTION_LOCATION = "com.q_rim.app8.ACTION_LOCATION";

  private static RunManagerSingleton runManagerSingleton;
  private Context appContext;
  private LocationManager locationManager;

  // private constructor forces users to use RunManager.get(Context)
  private RunManagerSingleton(Context appContext) {
    this.appContext = appContext;
    this.locationManager = (LocationManager)appContext.getSystemService(Context.LOCATION_SERVICE);
  }

  public static RunManagerSingleton get(Context c) {
    if (runManagerSingleton == null) {
      // Use the application context to avoid leaking activities
      runManagerSingleton = new RunManagerSingleton(c.getApplicationContext());
    }
    return runManagerSingleton;
  }

  private PendingIntent getLocationPendingIntent(boolean shouldCreate) {
    Intent broadcast = new Intent(ACTION_LOCATION);
    int flags = shouldCreate ? 0 : PendingIntent.FLAG_NO_CREATE;
    return PendingIntent.getBroadcast(this.appContext, 0, broadcast, flags);
  }

  public void startLocationUpdates() {
    String provider = LocationManager.GPS_PROVIDER;

    // Listing 33.10:  Getting the last known location
    // Get the last known location and broadcast it if you have one
    Location lastKnown = this.locationManager.getLastKnownLocation(provider);
    if (lastKnown != null) {
      // Reset the time to now
      lastKnown.setTime(System.currentTimeMillis());
      broadcastLocation(lastKnown);
    }

    // Start updates from the location manager
    PendingIntent pi = getLocationPendingIntent(true);
    this.locationManager.requestLocationUpdates(provider, 0, 0, pi);
  }

  // Listing 33.10:  Getting the last known location
  public void broadcastLocation(Location location) {
    Intent broadcast = new Intent(ACTION_LOCATION);
    broadcast.putExtra(LocationManager.KEY_LOCATION_CHANGED, location);
    this.appContext.sendBroadcast(broadcast);
  }

  public void stopLocationUpdates() {
    PendingIntent pi = getLocationPendingIntent(false);
    if (pi != null) {
      this.locationManager.removeUpdates(pi);
      pi.cancel();
    }
  }

  public boolean isTrackingRun() {
    return getLocationPendingIntent(false) != null;
  }
}
