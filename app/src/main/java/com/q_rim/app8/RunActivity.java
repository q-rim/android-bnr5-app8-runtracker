package com.q_rim.app8;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class RunActivity extends SingleFragmentActivity {

  @Override
  protected Fragment createFragment() {
    return new RunFragment();
  }
}
