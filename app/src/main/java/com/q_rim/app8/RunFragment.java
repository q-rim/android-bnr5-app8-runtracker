package com.q_rim.app8;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class RunFragment extends Fragment {

  private BroadcastReceiver locationReceiver = new LocationReceiver() {

    @Override
    protected void onLocationReceived(Context context, Location loc) {
      lastLocation = loc;
      if (isVisible())
        updateUI();
    }

    @Override
    protected void onProviderEnabledChanged(boolean enabled) {
      int toastText = enabled ? R.string.gps_enabled : R.string.gps_disabled;
      Toast.makeText(getActivity(), toastText, Toast.LENGTH_SHORT).show();
    }
  };

  private Run run;
  private Location lastLocation;
  private RunManagerSingleton runManager;

  private Button startButton, stopButton;
  private TextView startedTextView, latitudeTextView,
    longitutdeTextView, altitudeTextView, durationTextView;

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setRetainInstance(true);
    this.runManager = runManager.get(getActivity());
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_run, container, false);

    this.startedTextView = (TextView)view.findViewById(R.id.run_startedTextView);
    this.latitudeTextView = (TextView)view.findViewById(R.id.run_latitudeTextView);
    this.longitutdeTextView = (TextView)view.findViewById(R.id.run_longitudeTextView);
    this.altitudeTextView = (TextView)view.findViewById(R.id.run_altitudeTextView);
    this.durationTextView = (TextView)view.findViewById(R.id.run_durationTextView);

    this.startButton = (Button)view.findViewById(R.id.run_startButton);
    this.startButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        runManager.startLocationUpdates();
        run = new Run();
        updateUI();
      }
    });

    this.stopButton = (Button)view.findViewById(R.id.run_stopButton);
    this.stopButton.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        runManager.stopLocationUpdates();
        updateUI();
      }
    });

    updateUI();
    return view;
  }

  @Override
  public void onStart() {
    super.onStart();
    getActivity().registerReceiver(this.locationReceiver, new IntentFilter(RunManagerSingleton.ACTION_LOCATION));
  }

  @Override
  public void onStop() {
    getActivity().unregisterReceiver(this.locationReceiver);
    super.onStop();
  }

  private void updateUI() {
    boolean started = this.runManager.isTrackingRun();

    if (this.run != null)
      this.startedTextView.setText(this.run.getStartDate().toString());

    int durationSeconds = 0;
    if (this.run != null && this.lastLocation != null) {
      durationSeconds = this.run.getDurationSeconds(this.lastLocation.getTime());
      this.latitudeTextView.setText(Double.toString(this.lastLocation.getLatitude()));      // TextView - set latitude
      this.longitutdeTextView.setText(Double.toString(this.lastLocation.getLongitude()));   // TextView - set longitude
      this.altitudeTextView.setText(Double.toString(this.lastLocation.getAltitude()));      // TextView - set altitude
    }
    this.durationTextView.setText(Run.formatDuration(durationSeconds));                     // TextView - set duration

    this.startButton.setEnabled(!started);
    this.stopButton.setEnabled(started);
  }
}
